<?php

declare(strict_types=1);

namespace App;

use DateTime;
use Exception;
use DateInterval;

class SuperLogicaDate
{
    private const USA_DATE_FORMAT = 'm/d/Y';

    private DateTime $dateTime;

    protected static $datasPorExtenso = array(
        'January' => 'Janeiro', 'February' => 'Fevereiro', 'March' => 'Março', 'April' => 'Abril', 'mayfull' => 'Maio',
        'May' => 'Maio', 'June' => 'Junho', 'July' => 'Julho', 'August' => 'Agosto',
        'September' => 'Setembro', 'October' => 'Outubro', 'November' => 'Novembro', 'December' => 'Dezembro',
        'Jan' => 'Jan', 'Feb' => 'Fev', 'Mar' => 'Mar', 'Apr' => 'Abr', 'May' => 'Mai', 'Jun' => 'Jun',
        'Jul' => 'Jul', 'Aug' => 'Ago', 'Sep' => 'Set', 'Oct' => 'Out', 'Nov' => 'Nov', 'Dec' => 'Dez',
        'Sunday' => 'Domingo', 'Monday' => 'Segunda', 'Tuesday' => 'Terça',
        'Wednesday' => 'Quarta','Thursday' => 'Quinta', 'Friday' => 'Sexta', 'Saturday' => 'Sábado',
        'Sun' => 'Dom', 'Mon' => 'Seg', 'Tue' => 'Ter', 'Wed' => 'Qua', 'Thu' => 'Qui', 'Fri' => 'Sex', 'Sat' => 'Sab'
    );

    public function __construct($date = '', $format = '')
    {
        if ($date == '') {
            $date = date("m/d/Y H:i:s");
            $format = 'm/d/Y H:i:s';
        } elseif ($date instanceof SuperLogicaDate) {
            $this->dateTime = $date->getDateTime();
            return;
        } elseif (is_numeric($date)) {
            $this->setTimeStamp($date);
            return;
        } elseif (
            ($format == '') &&
            (is_string($date)) &&
            (strpos($date, '-') !== false) &&
            (preg_match("/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])\b(.+?)?$/", $date))
        ) {
            $format = 'Y-m-d';
            $format = $this->detectarHora($date, $format);
        } elseif ($format == '') {
            $format = self::USA_DATE_FORMAT;
            $format = $this->detectarHora($date, $format);
        }

        $datetime = static::createFromFormat($date, $format);

        if ($datetime === false) {
            throw new Exception("Formato de data inválido. Deveria estar no formato'$format'.");
        }

        $this->dateTime = $datetime;
    }

    private static function createFromFormat($date, $format = self::USA_DATE_FORMAT): DateTime | bool
    {
        if ($format == '') {
            $format = self::USA_DATE_FORMAT;
        }

        $datetime = DateTime::createFromFormat($format, $date);

        return ($datetime != null) ? $datetime : false;
    }

    public function getDateTime(): DateTime
    {
        return $this->dateTime;
    }

    protected function setTimeStamp($timestamp): void
    {
        $this->dateTime = new DateTime();
        $this->dateTime->setTimestamp($timestamp);
    }

    protected function detectarHora($date, $format): string | bool
    {
        $hourFormat =  $this->extrairFormatoData($date);
        return $hourFormat !== false ? $format . ' ' . $hourFormat : $format;
    }

    protected function extrairFormatoData($date): string | bool
    {
        $partDate = explode(' ', strtolower($date));

        $result = false;

        if (!array_key_exists(1, $partDate)) {
            return $result;
        }

        $string = $partDate[1];

        if (preg_match('/^\d{2}:\d{2}:\d{2}$/', $string)) {
            $result = 'H:i:s';
        }

        if (preg_match('/^\d{2}:\d{2}:\d{2}\.\d{3}$/', $string)) {
            $result =  'H:i:s.v';
        }

        return $result;
    }

    /**
     * Poderia ser substituida por formatação em pT_BR
     * Continuará assim para não mudar o comportamento da função.
     */
    public function toString($format = 'd/m/Y'): string
    {
        if (!$format) {
            $format = 'd/m/Y';
        }

        $date = $this->dateTime->format($format);

        if ($format == 'F' && $date == 'May') {
            $date = "mayfull";
        }

        if (preg_match('/(D|l|M|F)/', $format)) {
            foreach (self::$datasPorExtenso as $chave => $valor) {
                $date = str_replace($chave, $valor, $date);
            }
        }
        return $date;
    }

    public function addMonth($meses = 1): bool
    {
        if (!is_numeric($meses)) {
            return false;
        }

        $dateInterval = new DateInterval('P' .  $meses . 'M');
        $this->dateTime->add($dateInterval);
        return true;
    }

    public function subMonth($meses = 1): bool
    {
        if (!is_numeric($meses)) {
            return false;
        }

        $dateInterval = new DateInterval('P' .  $meses . 'M');
        $this->dateTime->sub($dateInterval);
        return true;
    }

    public function isDomingo(): bool
    {
        return $this->toString('l') === 'Domingo' ? true : false;
    }
}
