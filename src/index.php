<?php

declare(strict_types=1);

require 'app/SuperLogicaDate.php';

use App\SuperLogicaDate;

class Index
{
    public static function execute()
    {
        //Questão a
        $date = new SuperLogicaDate('03/01/2001', 'm/d/Y');
        echo $date->toString('Y-m-d') . PHP_EOL;
        
        //Questão B
        $date1 = new SuperLogicaDate('05/06/2001 17:16:18', 'm/d/Y H:i:s');
        echo $date1->toString('m/d/Y H:i:s') . PHP_EOL;
        $date2 = new SuperLogicaDate('05/06/2001 17:16:18', 'm/d/Y H:i:s');
        $date2->addMonth(3);
        echo $date2->toString('m/d/Y H:i:s') . PHP_EOL;
        $date2->subMonth(5);
        echo $date->toString('m/d/Y H:i:s') . PHP_EOL;

        //Questão C
        echo $date1->isDomingo() === true ? 'É Domingo' . PHP_EOL : 'Não é Domingo' . PHP_EOL ;
         

    }
}

Index::execute();
