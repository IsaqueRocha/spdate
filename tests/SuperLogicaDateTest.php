<?php

declare(strict_types=1);

namespace Tests;

use DateTime;
use ReflectionClass;
use App\SuperLogicaDate;
use PHPUnit\Framework\TestCase;

class SuperLogicaDateTest extends TestCase
{
    private const DATE_USA_FORMAT = 'm/d/Y';
    private const DATE_SQL_FORMAT = 'Y-m-d';
    private const DATETIME_USA_FORMAT = 'm/d/Y H:i:s';
    private const DATETIME_SQL_FORMAT = 'Y-m-d H:i:s';
    private const SQL_DATETIME = '2001-03-10 17:16:18';
    private const USA_DATETIME = '03/01/2001 17:16:18';
    private const DATETIME_MAY = '05/01/2001 17:16:18';

    private SuperLogicaDate $date;

    protected function setUp(): void
    {
        $this->date = new SuperLogicaDate(self::SQL_DATETIME, self::DATETIME_SQL_FORMAT);
    }

    public function testCanCreateDateWithoutParams()
    {
        $this->assertNotNull($this->date->getDateTime());
    }

    public function testCanCreateDateWithSqlFormat()
    {
        $this->date = new SuperLogicaDate(self::SQL_DATETIME, self::DATETIME_SQL_FORMAT);
        $this->assertNotNull($this->date->getDateTime());
        $this->assertEquals(self::SQL_DATETIME, $this->date->getDateTime()->format(self::DATETIME_SQL_FORMAT));
    }

    public function testCanCreateDateWithUsaFormat()
    {
        $this->date = new SuperLogicaDate(self::USA_DATETIME, self::DATETIME_USA_FORMAT);

        $this->assertNotNull($this->date->getDateTime());
        $this->assertEquals(self::USA_DATETIME, $this->date->getDateTime()->format(self::DATETIME_USA_FORMAT));
    }

    public function testCanCreateDateFromOtherSuperLogicaDate()
    {
        $date1 = new SuperLogicaDate();
        $this->assertNotNull($date1->getDateTime());

        $date2 = new SuperLogicaDate($date1);
        $this->assertNotNull($date2->getDateTime());

        $this->assertEquals($date1->getDateTime(), $date2->getDateTime());
    }

    public function testExtrairFormatoData()
    {
        $this->date = new SuperLogicaDate();

        $reflectionClass = new ReflectionClass(SuperLogicaDate::class);
        $reflectionMethod = $reflectionClass->getMethod('detectarHora');
        $reflectionMethod->setAccessible(true); //NOSONAR

        $result = $reflectionMethod->invokeArgs($this->date, ['2001-03-10 17:16:18', self::DATE_SQL_FORMAT]);
        $this->assertEquals(self::DATETIME_SQL_FORMAT, $result);

        $result = $reflectionMethod->invokeArgs($this->date, ['2001-03-10', self::DATE_SQL_FORMAT]);
        $this->assertEquals(self::DATE_SQL_FORMAT, $result);

        $result = $reflectionMethod->invokeArgs($this->date, [self::USA_DATETIME, self::DATE_USA_FORMAT]);
        $this->assertEquals(self::DATETIME_USA_FORMAT, $result);

        $result = $reflectionMethod->invokeArgs($this->date, ['03/01/2001', self::DATE_USA_FORMAT]);
        $this->assertEquals(self::DATE_USA_FORMAT, $result);
    }

    public function testSetTimeStamp()
    {
        $datetime = new DateTime();
        $this->date = new SuperLogicaDate();

        $reflectionClass = new ReflectionClass(SuperLogicaDate::class);
        $reflectionMethod = $reflectionClass->getMethod('setTimeStamp');
        $reflectionMethod->setAccessible(true); //NOSONAR

        $reflectionMethod->invokeArgs($this->date, [$datetime->getTimestamp()]);
        $result = $this->date->getDateTime();

        $this->assertEquals($datetime->getTimestamp(), $result->getTimestamp());
    }

    public function testToString()
    {
        $this->date = new SuperLogicaDate(self::DATETIME_MAY, self::DATETIME_USA_FORMAT);
        $this->assertEquals('Mai', $this->date->toString('M'));
        $this->assertEquals('Maio', $this->date->toString('F'));
        $this->assertEquals('05', $this->date->toString('m'));
        $this->assertEquals('Ter', $this->date->toString('D'));
        $this->assertEquals('01', $this->date->toString('d'));
        $this->assertEquals('Terça', $this->date->toString('l'));
    }

    public function testAddMonth()
    {
        $this->date = new SuperLogicaDate(self::DATETIME_MAY, self::DATETIME_USA_FORMAT);

        $this->date->addMonth(3);
        $this->assertEquals('08/01/2001 17:16:18', $this->date->getDateTime()->format(self::DATETIME_USA_FORMAT));
        $this->date->addMonth(5);
        $this->assertEquals('01/01/2002 17:16:18', $this->date->getDateTime()->format(self::DATETIME_USA_FORMAT));
        $this->date->addMonth(12);
        $this->assertEquals('01/01/2003 17:16:18', $this->date->getDateTime()->format(self::DATETIME_USA_FORMAT));
    }

    public function testSubMonth()
    {
        $this->date = new SuperLogicaDate(self::DATETIME_MAY, self::DATETIME_USA_FORMAT);

        $this->date->subMonth(3);
        $this->assertEquals('02/01/2001 17:16:18', $this->date->getDateTime()->format(self::DATETIME_USA_FORMAT));
        $this->date->subMonth(5);
        $this->assertEquals('09/01/2000 17:16:18', $this->date->getDateTime()->format(self::DATETIME_USA_FORMAT));
        $this->date->subMonth(12);
        $this->assertEquals('09/01/1999 17:16:18', $this->date->getDateTime()->format(self::DATETIME_USA_FORMAT));
    }

    public function testIsDomingo()
    {
        $this->date = new SuperLogicaDate(self::DATETIME_MAY, self::DATETIME_USA_FORMAT);
        $this->assertFalse($this->date->isDomingo());

        $this->date = new SuperLogicaDate('05/06/2001 17:16:18', self::DATETIME_USA_FORMAT);
        $this->assertTrue($this->date->isDomingo());
    }
}
